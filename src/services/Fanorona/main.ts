import { COLOUMN, ROW } from "./Constante/Constante";
import { Player } from "./Player";
import { Pion, Type, IPosition, IMove } from "./_type";

const IA = new Player(Pion.BLACK,Type.ALGO);
const PLAYER = new Player(Pion.WHITE,Type.PLAYER);
console.table(IA.board);
// player 
console.log("Player")
const choice: IPosition = { x: 3, y:1  };
const possibleMove = PLAYER.choiceMove(PLAYER.board,choice) as IMove[];
console.log(possibleMove);
let step = PLAYER.canCapture(PLAYER.board, possibleMove[3],PLAYER.pion);
let newBoard = PLAYER.makeMove(PLAYER.board, possibleMove[3], step,PLAYER.pion,false);

IA.board =  newBoard.map((e)=>e.slice());
PLAYER.board = newBoard.map((e)=>e.slice()); 
console.table(IA.board);



// IA
console.log("IA")
let bestScore = -Infinity;
let bestMove : IMove={to:{x:0,y:0},from:{x:0,y:0}};



for (let row = 0; row < ROW; row++) {
    for (let col = 0; col < COLOUMN; col++) {
        if (IA.board[row][col] === IA.pion) {
            const currentPlayer: IPosition = { x: row, y: col };
            const moves = IA.generateMoves(IA.board, currentPlayer, IA.pion);
            bestMove = IA.GetBestMoveForCurrentPion(IA.board,moves).bestMove;
        }
    }
}
let numberOfOpponent = IA.GetNumberOfOpponent()
step = IA.canCapture(IA.board, bestMove,IA.pion);
IA.board = IA.makeMove(IA.board,bestMove, step,IA.pion,false);

let NewNumberOfOpponent= IA.GetNumberOfOpponent();
while(NewNumberOfOpponent!=numberOfOpponent){
    let currentPlayer = bestMove.to
    bestScore = -Infinity;
    const moves = IA.generateMoves(IA.board, currentPlayer, IA.pion);
    bestMove = IA.GetBestMoveForCurrentPion(IA.board,moves).bestMove;
    step = IA.canCapture(IA.board, bestMove,IA.pion);
    IA.board = IA.makeMove(IA.board,bestMove, step,IA.pion,false);
    numberOfOpponent = NewNumberOfOpponent;
    NewNumberOfOpponent = IA.GetNumberOfOpponent();
}
