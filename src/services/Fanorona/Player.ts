import { moveAi } from "../algo/algo.servive";
import { COLOUMN, ROW } from "./Constante/Constante";
import {
  IData,
  IMove,
  INextMove,
  IPosition,
  IposCaptAndAsp,
  Matrix,
  Pion,
  Type,
} from "./_type";
import { Fanorona } from "./fanorona";

export class Player extends Fanorona {
  pion: Pion;
  nom: Type;
  constructor(pion: Pion, nom?: Type) {
    super();
    this.pion = pion;
    this.nom = nom as Type;
  }
  choiceMove(
    grid: Matrix<string>,
    choice: IPosition = { x: 0, y: 0 }
  ): IMove | IMove[] | any {
    if (this.nom == Type.ALGO) {
      // return this.MinMax(grid,6,-Infinity,Infinity,false) as IMove;*
      return;
    } else {
      return this.generateMoves(grid, choice, this.pion);
    }
  }
  GetNumberOfOpponent(): number {
    let opponent = 0;
    for (let row = 0; row < ROW; row++) {
      for (let col = 0; col < COLOUMN; col++) {
        const current = this.board[row][col];
        if (current != this.pion && current != Pion.EMPTY) {
          opponent += 1;
        }
      }
    }
    return opponent;
  }
  GetBestMoveForCurrentPion(board: Matrix<string>, moves: IMove[]): INextMove {
    let bestMove: IMove = { to: { x: 0, y: 0 }, from: { x: 0, y: 0 } };
    let bestScore = -Infinity;
    for (const move of moves) {
      let newBoard = board.map((e) => e.slice());
      const step = this.canCapture(newBoard, move, this.pion);
      newBoard = this.makeMove(newBoard, move, step, this.pion, false);
      const { score } = this.MinMax(newBoard, 5, -Infinity, Infinity, false);
      if (score > bestScore) {
        bestScore = score;
        bestMove = move;
      }
    }
    return { bestMove, bestScore };
  }

  async MoveAI(board: Matrix<string>) {
    const data : IData ={
      board:board,
      pion:this.pion
    }
    return  await moveAi(data)
  }
}
