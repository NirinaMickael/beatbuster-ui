import { IPosition } from "../_type";

export const DIRECTION  :  IPosition[] = [
    {
        x:-1,
        y:1
    },
    {
        x:1,
        y:0
    },
    {
        x:-1,
        y:0
    },
    {
        x:0,
        y:1
    },
    {
        x:0,
        y:-1
    },
    {
        x:1,
        y:1
    },
    {
        x:1,
        y:-1
    },
    {
        x:-1,
        y:-1
    }
]

export const DIRECTIONKELY  :  IPosition[] = [
    {
        x:0,
        y:1
    },
    {
        x:0,
        y:-1
    },
    {
        x:-1,
        y:0
    },
    {
        x:1,
        y:0
    }
]

export const COLOUMN = 9
export const  ROW = 5

