import { API } from "@/utils/api"
import { IData, IMove } from "../Fanorona/_type"

export const moveAi= async (data:IData)=>{
    try {
        return (await API.post<IMove[]>('fanorona/move-ai',data)).data
    } catch (error) {
        return error
    }
}