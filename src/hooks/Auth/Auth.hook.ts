import { createContext, useContext, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { UseLocalStorage } from "../storage/UseLocalStorage.hook";
import { IUser } from "@/@schema";
interface AuthContextProps {
  user: IUser | null;
  login: (data:IUser) => Promise<void>;
  logout: () => void
}
const AuthContext = createContext<AuthContextProps>({
  user: null,
  login: async () => {
    return Promise.resolve()
  },
  logout: () => { }
});

export const AuthProvider = ({ children }: any) => {
  const [user, setUser] = UseLocalStorage("user", null);
  const navigate = useNavigate();

  const login = async (data: any) => {
    setUser(data);
    navigate("/");
  };

  const logout = () => {
    setUser(null);
    navigate("/", { replace: true });
  };

  const value = useMemo<AuthContextProps>(
    () => ({
      user,
      login,
      logout,
    }),
    [user]
  );
  return <AuthContext.Provider value={ value }> { children } < /AuthContext.Provider>;
};

export const useAuth = () => {
  return useContext(AuthContext);
};