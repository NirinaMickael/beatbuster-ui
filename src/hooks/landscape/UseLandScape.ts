
import { useState, useEffect } from "react"
const useHookLandscape = () => {
    const [isLocked, setIsLocked] = useState(false);

    useEffect(() => {
        const enforceLandscapeOrientation = () => {
            if (isLocked && screen.orientation) {
                (screen.orientation as any).lock('landscape')
                    .then(() => console.log('Screen orientation locked to landscape'))
                    .catch((error: any) => {
                        console.error('Could not lock screen orientation', error);
                    });
            } else {
                if (screen.orientation) {
                    screen.orientation.unlock();
                }
            }
        };

        enforceLandscapeOrientation();

        return () => {
            if (screen.orientation) {
                screen.orientation.unlock();
            }
        };
    }, [isLocked]);

    const toggleOrientationLock = () => {
        setIsLocked(!isLocked);
    };

    return { isLocked, toggleOrientationLock };
};

export default useHookLandscape;