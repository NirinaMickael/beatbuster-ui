interface LayoutProps {
    children: React.ReactNode;
}
export const Layout: React.FC<LayoutProps> = ({ children }) => {
    return (
        <div
            className="h-full w-full  absolute m-auto bg-slate-600"
        >
            {children}
        </div>
    )
}
