import { COLOUMN, ROW } from "@/services/Fanorona/Constante/Constante";
import { Player } from "@/services/Fanorona/Player";
import { IBFS, IMove, IPosition, Matrix, Pion } from "@/services/Fanorona/_type";


export class FanoronaService {
    player: Player;
    constructor(player: Player) {
        this.player = player;
    }
    public GetAllPossiblePath(board: Matrix<string>, pos: IPosition) {
        return this.player.findBestCapture(board, pos, this.player.pion);
    }
    public Movelength(paths: IBFS[], pos_: IPosition) {
        const move = paths.filter((ele, _) => {
            return ele.path.findIndex((pos, _) => {
                return pos.x == pos_.x && pos.y == pos_.y;
            }) == 1 && !(ele.noeud.x == pos_.x && ele.noeud.y == pos_.y)
        })
        return move.length ? move[0].path : move
    }
    public GetOppotnentLength(board:Matrix<string>){
        let sum=0;
        for (let row = 0; row < ROW; row++) {
            for (let col = 0; col < COLOUMN; col++) {
                if(board[row][col]==this.player.pion){
                    sum+=1
                }
            }
        }
        return sum;
    }
    public GetNextMove(moves:IMove[],pos:IPosition){
        return moves.filter((move,idx)=>{
            return move.to.x == pos.x && move.to.y==pos.y;
        })[0];
    }
    
}