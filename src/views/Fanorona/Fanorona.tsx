import "./Fanorona.scss"
import useHookLandscape from "@/hooks/landscape/UseLandScape";
import React from "react";
import { Plateaux } from "./shared/plateaux";
export const Fanorona: React.FC = () => {
    const { isLocked, toggleOrientationLock } = useHookLandscape();
    console.log(isLocked);
    return (

        <div
            id="container"
        >
            <button onClick={() => toggleOrientationLock()} >edit</button>
            <div
                className="plateau justify-self-center"
            >
                <Plateaux />

            </div>
        </div>
    )
}