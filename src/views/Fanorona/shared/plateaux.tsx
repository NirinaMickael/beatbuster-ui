import { Player } from "@/services/Fanorona/Player";
import { IBFS, IMove, IPosition, Matrix, Pion, Type, Vector } from "@/services/Fanorona/_type"
import { CirclePosition, LigneData } from "../Constante/data.mock";
import { useEffect, useState } from "react";
import { FanoronaService } from "../service/fanorona.service";
import { moveAi } from "@/services/algo/algo.servive";
const PLAYER = new Player(Pion.WHITE, Type.PLAYER);
const IA = new Player(Pion.BLACK, Type.ALGO);
const PlayerService = new FanoronaService(PLAYER);
const IAService = new FanoronaService(IA);

interface PlateauxProps {
    board: Matrix<string>
}

export const Plateaux: React.FC = () => {
    const [oppotenetLenght, setOppotenetLenght] = useState<number>(22);
    const [possibleNextMove, SetpossibleNextMove] = useState<IMove[]>([]);
    const [currentPosPion, SetCurrentPosition] = useState<IPosition | null>(null);
    const [allPath, SetAllPath] = useState<IPosition[]>([]);
    const [BOARD, SetBoard] = useState<Matrix<string>>(IA.board);
    const [isPlayerTurn, setPlayerTurn] = useState<boolean>(true);


    useEffect(() => {
        const oppotenetLenght = IAService.GetOppotnentLength(BOARD);
        setOppotenetLenght(oppotenetLenght);
    }, []);

    const ActiveOrDeactivePion = (board: Matrix<string>, possibleNextMove_: IMove[], isActive: boolean) => {
        const newBoard = board;
        // on va remettre les places ou ce pion peut deplacer en ACTIVE
        for (const { to, from } of possibleNextMove_) {
            newBoard[to.x][to.y] = isActive ? Pion.ACTIVE : Pion.EMPTY;
        }
        return newBoard;
    }
    const Run = (pos: IPosition) => {
        if (isPlayerTurn) {
            const currentPion = BOARD[pos.x][pos.y];
            if (currentPion === PLAYER.pion) {
                SetAllPath((curr) => {
                    return [...curr, pos]
                });
                if (currentPosPion == null || currentPosPion.x == pos.x && currentPosPion.y == pos.y) {
                    let possibleNextMove_ = PLAYER.choiceMove(BOARD, pos) as IMove[];
                    possibleNextMove_ = possibleNextMove_.filter((move, _) => {
                        return allPath.findIndex((path, id) => move.to.x == path.x && move.to.y == path.y) == -1
                    });


                    // on va tester si le pion choisi par le joueur peut deplacer
                    // const { allPath, bestPast } = PlayerService.GetAllPossiblePath(BOARD, pos);
                    // SetAllPath(allPath);
                    if (possibleNextMove_.length) {
                        // on va modifiler le state du mouvement possible ;
                        SetpossibleNextMove(possibleNextMove_);
                        // on va remettre les places ou ce pion peut deplacer en ACTIVE
                        const newBoard = ActiveOrDeactivePion(BOARD, possibleNextMove_, true);
                        SetBoard(newBoard);
                    }
                } else {
                    console.log("tsy mety")
                }
            } else if (currentPion === Pion.ACTIVE) {
                // Donc le joueur
                SetCurrentPosition(pos);
                SetAllPath((curr) => {
                    return [...curr, pos];
                })
                console.log(oppotenetLenght);
                const nexMove = PlayerService.GetNextMove(possibleNextMove, pos);
                const step = PLAYER.canCapture(BOARD, nexMove, PLAYER.pion);
                let newBoard = PLAYER.makeMove(BOARD, nexMove, step, PLAYER.pion);
                newBoard = ActiveOrDeactivePion(newBoard, possibleNextMove, false);
                newBoard[pos.x][pos.y] = PLAYER.pion;

                SetBoard(newBoard);
                const newOppotnentLength = IAService.GetOppotnentLength(newBoard);
                setOppotenetLenght(newOppotnentLength);
                if (newOppotnentLength == oppotenetLenght) {
                    setPlayerTurn(false);
                    SetCurrentPosition(null);
                    SetAllPath([]);
                    AiRun();
                } else {

                }
            }
        }
    }

    const AiRun = async() => {
        //IA.MoveAI(BOARD);
        // can capture t sy mety tsara
        try {
            const MoveList = await IA.MoveAI(BOARD);
            console.log("==+>",MoveList);
        } catch (error) {
            
        }
    }
    return (
        <svg width="500" height="250" viewBox="0 0 722 352" fill="none" xmlns="http://www.w3.org/2000/svg">

            {
                LigneData.map((ligne, idx) => {
                    return (
                        <>
                            <line {...ligne} key={idx} />
                            {
                                idx == 6 &&
                                (
                                    <rect x="11" y="12" width="698" height="328" fill="#D9D9D9" fillOpacity="0.34" stroke="black" strokeWidth="2" />
                                )
                            }
                        </>
                    )
                })
            }
            {
                BOARD.flat().map((pion, idx) => {
                    const color = pion == "1" ? "#222831" : pion == "0" ? "#FEFEFE" : pion == "2" ? "#FCDC2A" : "#C7003978";
                    const position = {
                        y: idx % 9,
                        x: Math.floor(idx / 9)
                    }
                    return <circle {...CirclePosition[idx]} r="10" fill={color} key={idx} onClick={(e) => Run(position)} />
                })
            }
        </svg>

    )
}