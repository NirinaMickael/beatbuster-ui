export interface IPostionBlock {
  cx: number,
  cy: number
}

export const CirclePosition: IPostionBlock[] = [
  {
    "cx": 13,
    "cy": 10
  },
  {
    "cx": 95,
    "cy": 11
  },
  {
    "cx": 185,
    "cy": 13
  },
  {
    "cx": 271,
    "cy": 11
  },
  {
    "cx": 360,
    "cy": 13
  },
  {
    "cx": 450,
    "cy": 13
  },
  {
    "cx": 532,
    "cy": 13
  },
  {
    "cx": 623,
    "cy": 13
  },
  {
    "cx": 709,
    "cy": 13
  },
  {
    "cx": 10,
    "cy": 91
  },
  {
    "cx": 97,
    "cy": 94
  },
  {
    "cx": 184,
    "cy": 93
  },
  {
    "cx": 272,
    "cy": 91
  },
  {
    "cx": 361,
    "cy": 91
  },
  {
    "cx": 447,
    "cy": 91
  },
  {
    "cx": 534,
    "cy": 92
  },
  {
    "cx": 623,
    "cy": 91
  },
  {
    "cx": 710,
    "cy": 91
  },
  {
    "cx": 10,
    "cy": 175
  },
  {
    "cx": 97,
    "cy": 177
  },
  {
    "cx": 186,
    "cy": 173
  },
  {
    "cx": 273,
    "cy": 176
  },
  {
    "cx": 362,
    "cy": 175
  },
  {
    "cx": 449,
    "cy": 171
  },
  {
    "cx": 533,
    "cy": 175
  },
  {
    "cx": 625,
    "cy": 174
  },
  {
    "cx": 712,
    "cy": 174
  },
  {
    "cx": 13,
    "cy": 258
  },
  {
    "cx": 97,
    "cy": 258
  },
  {
    "cx": 186,
    "cy": 260
  },
  {
    "cx": 273,
    "cy": 258
  },
  {
    "cx": 364,
    "cy": 260
  },
  {
    "cx": 450,
    "cy": 260
  },
  {
    "cx": 534,
    "cy": 259
  },
  {
    "cx": 623,
    "cy": 260
  },
  {
    "cx": 710,
    "cy": 258
  },
  {
    "cx": 13,
    "cy": 342
  },
  {
    "cx": 100,
    "cy": 341
  },
  {
    "cx": 190,
    "cy": 338
  },
  {
    "cx": 275,
    "cy": 337
  },
  {
    "cx": 364,
    "cy": 337
  },
  {
    "cx": 448,
    "cy": 338
  },
  {
    "cx": 539,
    "cy": 337
  },
  {
    "cx": 625,
    "cy": 338
  },
  {
    "cx": 710,
    "cy": 338
  },

]

export const LigneData = [
  {
    "x1": "96",
    "x2": "98",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "10.9939",
    "y2": "340.994"
  },
  {
    "x1": "185",
    "x2": "187",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "10.9939",
    "y2": "340.994"
  },
  {
    "x1": "272",
    "x2": "274",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "10.9939",
    "y2": "340.994"
  },
  {
    "x1": "361",
    "x2": "363",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "10.9939",
    "y2": "340.994"
  },
  {
    "x1": "623",
    "x2": "625",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "10.9939",
    "y2": "340.994"
  },
  {
    "x1": "448",
    "x2": "450",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "10.9939",
    "y2": "340.994"
  },
  {
    "x1": "533",
    "x2": "535",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "9.99394",
    "y2": "339.994"
  },
  {
    "x1": "",
    "x2": "",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "0.34",
    "y1": "",
    "y2": ""
  },
  {
    "x1": "9.99857",
    "x2": "709.999",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "175",
    "y2": "174"
  },
  {
    "x1": "9.99714",
    "x2": "709.997",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "92",
    "y2": "90"
  },
  {
    "x1": "9.99714",
    "x2": "709.997",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "259",
    "y2": "257"
  },
  {
    "x1": "11.6816",
    "x2": "362.682",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "12.2683",
    "y2": "339.268"
  },
  {
    "x1": "185.679",
    "x2": "539.679",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "12.2654",
    "y2": "339.265"
  },
  {
    "x1": "185.679",
    "x2": "539.679",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "12.2654",
    "y2": "339.265"
  },
  {
    "x1": "360.684",
    "x2": "710.684",
    "stroke": "#1E1E1E",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "12.2703",
    "y2": "340.27"
  },
  {
    "x1": "534.677",
    "x2": "710.677",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "12.2642",
    "y2": "174.264"
  },
  {
    "x1": "11.6778",
    "x2": "190.678",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "175.265",
    "y2": "340.265"
  },
  {
    "x1": "13.31",
    "x2": "184.31",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "175.276",
    "y2": "12.2762"
  },
  {
    "x1": "13.31",
    "x2": "184.31",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "175.276",
    "y2": "12.2762"
  },
  {
    "x1": "13.31",
    "x2": "184.31",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "340.276",
    "y2": "177.276"
  },
  {
    "x1": "190.31",
    "x2": "361.31",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "174.276",
    "y2": "11.2762"
  },
  {
    "x1": "188.31",
    "x2": "359.31",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "337.276",
    "y2": "174.276"
  },
  {
    "x1": "363.31",
    "x2": "534.31",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "172.276",
    "y2": "9.27617"
  },
  {
    "x1": "13.31",
    "x2": "184.31",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "175.276",
    "y2": "12.2762"
  },
  {
    "x1": "364.31",
    "x2": "535.31",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "336.276",
    "y2": "173.276"
  },
  {
    "x1": "535.31",
    "x2": "706.31",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "175.276",
    "y2": "12.2762"
  },
  {
    "x1": "535.31",
    "x2": "706.31",
    "stroke": "black",
    "strokeWidth": "2",
    "fillOpacity": "",
    "y1": "339.276",
    "y2": "176.276"
  }
]