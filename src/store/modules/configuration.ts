import { combineReducers } from "@reduxjs/toolkit";
import { UserReducer } from "./user/User.reducers";

const rootReducer = combineReducers(
    {
        user : UserReducer
    }
)
export default rootReducer;
export type RootState = ReturnType<typeof rootReducer>;