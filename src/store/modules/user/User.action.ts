const USER = "USER"

import { IUser } from "@/@schema";
import { CreateUserSerivce, getUserService } from "@/services/user/User.service";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const userLoadRequested = createAsyncThunk(
    `${USER}/load request`,
    async (_, thunkAPI) => {
        try {
            const response = await getUserService(thunkAPI);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(error);
        }
    }
)


export const userSaveRequested = createAsyncThunk(
    `${USER}/save request`,
    async (data: IUser, thunkAPI) => {
        try {
            const response = await CreateUserSerivce(data, thunkAPI);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(error);
        }
    }
)


