import { IUser } from "@/@schema";
import { EntityAdapter, createEntityAdapter, createReducer } from "@reduxjs/toolkit";
import { UserState } from "./User.state";
import { userLoadRequested } from "./User.action";

export const UserAdapter: EntityAdapter<IUser, any> = createEntityAdapter(
    {
        selectId: (entry) => entry.id
    }
)

const initialState: UserState = UserAdapter.getInitialState(
    {
        loading: false,
        saving: false,
        errorMessage: "",
        user: undefined,
    }
)


export const UserReducer = createReducer(initialState,
    (builder) => {
        builder
            .addCase(userLoadRequested.pending, (state) => {
                return {
                    ...state,
                    loading: true,
                    errorMessage: ""
                }
            })
            .addCase(userLoadRequested.fulfilled, (state, action) => {
                UserAdapter.setAll(
                    state,
                    action.payload
                )
                state.loading = false;
                state.errorMessage = "";
            })
            .addCase(userLoadRequested.rejected, (state, action: any) => {
                state.loading = false;
                state.errorMessage = action?.error?.message || "an error occurerd"
            })
    }
)