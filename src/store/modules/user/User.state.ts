import { IUser } from "@/@schema";
import { EntityState } from "@reduxjs/toolkit";

export interface UserState extends EntityState<IUser,any>{
    loading : boolean;
    errorMessage : string ;
    saving : boolean;
    user : IUser | undefined;
}