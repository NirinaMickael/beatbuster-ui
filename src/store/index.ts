import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./modules/configuration";
import { thunk } from "redux-thunk";
const store = configureStore(
    {
        reducer: rootReducer,
        middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thunk),
    }
)
export default store;