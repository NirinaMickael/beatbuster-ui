
import './App.scss'
import { Fanorona } from './views/Fanorona/Fanorona';
import { Layout } from './views/layout/Layout';
function App() {
  return (
    <Layout>
      <Fanorona />
    </Layout>
  )
}

export default App
