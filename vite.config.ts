import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import path from "path"
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src/"),
      "@components": `${path.resolve(__dirname, "./src/components/")}`,
      "@assets": `${path.resolve(__dirname, "./assets/")}`,
      "@hooks/*": `${path.resolve(__dirname, "./hooks/")}`,
      "@services/*": `${path.resolve(__dirname, "./services/")}`,
      "@store/*": `${path.resolve(__dirname, "./store/")}`,
      "@utils/*": `${path.resolve(__dirname, "./utils/")}`,
      "@views/*": `${path.resolve(__dirname, "./views/")}`
    },
  },
})
